# GITHUB SEARCH CASE STUDY


### This app has been designed based on the user flow below:

- User runs the app
- User sees a search form and radio buttons - according to document specification
- User searches the endpoints /search/users?q=susan or /search/users?q=facebook+type:org
- A list of users or organizations is displayed with their names and avatars.
- User clicks on an organization or avatar to view their details.
- User sees a loading screen and the results showing additional details like name, public repos, biography, location, created at, public gists, followers, users following the viewed item.
- Users have the ability to go back to the search page. 

## Assumptions and Decisions

- I assumed that the app should implement a performant flow and not fetch data that is currently not needed or useful. 
  - An example is over-fetching user details that may not be needed. My decision with performance and data fetching in mind is to only fetch the user details when needed. This also made me implement caching the details of a user’s visited profile so as to serve these details from the cache for subsequent visits to the same user’s page.
- I assume that when the app is hosted, the search result url could possibly be shared to a different system. My decision to cater for this scenario is to compose a url that can be searched regardless of where it’s opened.
- I also assumed that this is a small app and thus, testing was minimal; caching the list of users was also not done but can be achieved.
- Another assumption I made is that if the app is refreshed when viewing a single user’s profile, and the back button on the profile is clicked, the previous search for users or organizations should not exist anymore.
- I assumed that the request results are best paginated for a better UI flow, this drove my decision to implement pagination.

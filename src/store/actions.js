
import { pageLimit, searchTypes } from '../utils';
import {
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_FAILURE,
  SET_SEARCH_PARAMS,
  SET_USERNAME,
  LOADING } from './index'


const showLoader = () => ({
  type: LOADING,
});

export const setSearchParams = (searchParams) => ({
  type: SET_SEARCH_PARAMS,
  searchParams
});

const getUsersActionSuccess = (payload) => ({
  type: GET_USERS_SUCCESS,
  payload
});

const getUsersActionFailure = message => ({
  type: GET_USERS_FAILURE,
  loading: false,
  message
});

export const setUsername = username => ({
  type: SET_USERNAME,
  username
})

export const fetchUserDetailsFailure = message => ({
  type: FETCH_USER_DETAILS_FAILURE,
  message
});

export const fetchUserDetailsSuccess = userDetails => ({
  type: FETCH_USER_DETAILS_SUCCESS,
  userDetails
});


// Redux Thunks
export const getUsers = payload => async dispatch => {
    const { searchQuery, pageNumber, reset, loadingMore } = payload

    dispatch(showLoader())
    const orgSearch = searchQuery.searchType === searchTypes.orgs.id
    const baseAPI = `https://api.github.com/search/users?per_page=${pageLimit.perPage}&page=${pageNumber}&q`
    const url = orgSearch ?
      `${baseAPI}=${searchQuery.searchText}+type:org` : `${baseAPI}=${searchQuery.searchText}`
    const response = await fetch(url)
    const result = await response.json()

    if (result.message) {
     return dispatch(getUsersActionFailure(result.message))
    } 
   
   return setTimeout(() => {
      return dispatch(getUsersActionSuccess({
        users:result.items, 
        totalCount: result.total_count,
        reset,
        pageNumber,
        loadingMore,
        searchQuery}))
    }, 2000)

}


export const fetchDetails = username => async dispatch => {

  dispatch(showLoader())
  const url = `https://api.github.com/users/${username}`

  const response = await fetch(url)
  const result = await response.json()

  if (result.message) {
    return dispatch(fetchUserDetailsFailure(result.message))
   } 

  setTimeout(()=> {
    return dispatch(fetchUserDetailsSuccess({...result, username}))
  }, 2000)
}

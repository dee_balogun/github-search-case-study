import { pageLimit } from '../utils';
import {
  GET_USERS_FAILURE,
  LOADING,
  GET_USERS_SUCCESS,
  SET_SEARCH_PARAMS,
  FETCH_USER_DETAILS_SUCCESS, 
  SET_USERNAME,
  FETCH_USER_DETAILS_FAILURE} from './index'

  const defaultState = {
    searchParams: {
      searchQuery: {},
      perPage: pageLimit.perPage,
      pageNumber: pageLimit.pageNumber,
    },
    userDetails: {},
    users: [],
    loadNew: true
  }

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_USERS_SUCCESS:
      return {
        ...state, 
        users: action.payload.reset ? action.payload.users : [
          ...action.payload.users,
          ...state.users
        ],
        totalCount: action.payload.totalCount,
        message: null,
        loading: false
      };
    case GET_USERS_FAILURE:
      return {
        ...state, 
        message: action.message,
        loading: false
      };
    case FETCH_USER_DETAILS_FAILURE:
      return {
        ...state, 
        message: action.message,
        loading: false
      };
    case SET_SEARCH_PARAMS:
      return {
        ...state, 
        searchParams: action.searchParams,
        loadNew: true
      };
    case SET_USERNAME: 
      return {
        ...state,
        currentUsername: action.username,
        loadNew: false
      }
    case FETCH_USER_DETAILS_SUCCESS:
      return {
        ...state, 
        loading: false,
        userDetails: {
          ...state.userDetails,
          [action.userDetails.username]: action.userDetails
        }
      };
    case LOADING:
      return {
        ...state, 
        loading: true,
        message: null
      };
    default:
      return state;
  }
};

export default reducer
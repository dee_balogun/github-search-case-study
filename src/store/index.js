import { applyMiddleware, compose } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import reducers from './reducer';

export const GET_USERS_FAILURE = 'GET_USERS_FAILURE'
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS'
export const FETCH_USER_DETAILS_SUCCESS = 'FETCH_USER_DETAILS_SUCCESS'
export const FETCH_USER_DETAILS_FAILURE = 'FETCH_USER_DETAILS_FAILURE'
export const LOADING = 'LOADING'
export const SET_SEARCH_PARAMS = 'SET_SEARCH_PARAMS'
export const SET_USERNAME = 'SET_USERNAME'

const middleWare = compose(applyMiddleware(thunk),
window.devToolsExtension && process.env.NODE_ENV === 'development'
  ?
  window.devToolsExtension() : f => f)
const store = configureStore({
    reducer: reducers,
    initialState: {},
    middleWare
  });

export default store;
export const searchTypes = {
  users: { id: 'users', label: 'Users'},
  orgs: { id: 'orgs', label: 'Organizations'}
}

export const pageLimit = {
  perPage: 20,
  pageNumber: 1
}

export const formatKey = key => {
  let result = '';
  key.split('_').forEach(current => {
    result += ` ${current[0].toUpperCase()}${current.substring(1)}`
  })
  return result
}

export const breakPoints = ['450', '600', '800']
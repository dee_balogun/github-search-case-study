import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  useRoutes,
  useLocation,
  useNavigate 
} from 'react-router-dom'

import ErrorBoundary from './HOC/ErrorBoundary'
import store from './store';
import Home from './views/Home';
import Profile from './views/Profile';
import Search from './views/Search';

const AppRoutes = () => { 
  const location = useLocation()
  const navigate = useNavigate()
  
  useEffect(() => {
    if (location.pathname === '/') {
      navigate('search')
    }
  }, [location.pathname, navigate])
  
  return useRoutes([
    { 
      path: 'search',
      element:<Home/>,
      children: [
        {
          path: ':searchType/:searchText',
          element:<Search/>,
        },
      ]
    },
    {
      path: '/:searchType/:username',
      element:<Profile/>,
    },
    {
      path: '*',
      element:<h1>Invalid Route</h1>,
    }
  ])
}

const App  = ()  => {
  return (
    <Provider store={store}>
      <ErrorBoundary>
        <Router>
          <AppRoutes/>
        </Router>
      </ErrorBoundary>
    </Provider>
  );
}

export default React.memo(App);

const strings = {
    githubSearch: "Github Search",
    appDescription: "This app searches the Github api for users or Organiztions",
    fetching : (name) => `Fetching ${name}'s profile`,
    errorLoadingUsers: 'Cannot fetch users now. Please try again.',
    noResult: 'No result for your search',
    loadingText: 'Processing request...',
    resultCountText: (usersCount, totalCount) => `Showing ${usersCount} of ${totalCount} results `,
    next: 'Next',
    appError: 'An error occured',
    searchAgain: 'Search again',
    errorFetchingProofile: 'Profile could not be fetched!',
    selectFieldToSearch: 'Select a field to search',
    search: 'Search'
}

export default strings
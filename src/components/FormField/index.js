import React from "react";
import { StyledSection, StyledRadioLabel, StyledLabel } from './styled'
import PropTypes from 'prop-types'

const radio = 'radio'

const FormField = ({
  type,
  name,
  value,
  label,
  onChange,
  direction,
  checked,
  placeholder,
  ...rest
}) => {
  return(
    <StyledSection direction={direction}>
        {
          type === radio ?
          <StyledRadioLabel>
            <input
              aria-label={label}
              tabIndex="0"
              type={radio}
              onChange={onChange} 
              name={name}
              value={value}
              checked={checked}
              {...rest}
            />{label}
          </StyledRadioLabel>
          :
          <>
            {label && <StyledLabel>{label}</StyledLabel>}
            <input
              type={type}
              aria-label={label || ''}
              tabIndex="0"
              name={name}
              onChange={onChange}
              placeholder={placeholder} 
              value={value}
              {...rest}
              />
          </>
        }
</StyledSection>
  )
}

FormField.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]).isRequired,
  onChange: PropTypes.func.isRequired,
  direction: PropTypes.oneOf(['row', 'column']),
  label: PropTypes.string,
  checked: PropTypes.bool,
  placeholder: PropTypes.string,
}

FormField.defaultProps = {
  label: '',
  placeholder: '',
  checked: false
}

export default FormField
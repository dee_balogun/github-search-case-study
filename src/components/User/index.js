import React, {  useCallback } from "react";
import { useNavigate } from 'react-router-dom';
import {
  StyledArticle,
  StyledButton,
  StyledDetailsSection,
  StyledFigure,
  StyledFigureCaption,
  StyledH4,
  StyledImage,
  StyledP
} from "./styled";
import PropTypes from 'prop-types'
import { formatKey } from "../../utils";
import { useSelector } from "react-redux";
import strings from "../../locales/strings";

const User = ({
  avatar_url, 
  login,
  otherDetails,
  userError,
  showDetails 
}) => {
  
  const navigate = useNavigate()
  const { searchParams } = useSelector(state => state)

  const goBack = useCallback(() => {

    // if they refresh, we lose data so just start again
    if (!searchParams.searchQuery.searchType) {
      navigate('/search')

      return
    }
    navigate({
      pathname: `/search/${searchParams.searchQuery.searchType}/${searchParams.searchQuery.searchText}`,
      search: `?pageNumber=${searchParams.pageNumber}`
      })
  }, [searchParams, navigate])


  return(
    <>
      <StyledFigure  tabindex='0'>
        <StyledImage
          alt={login}
          src={avatar_url}/>
          <StyledFigureCaption >{login}</StyledFigureCaption>
      </StyledFigure>
      {showDetails && <StyledDetailsSection>
        <StyledButton
          aria-label="Search again"
          tabindex='0'
          name='search'
          onClick={goBack}
          >{strings.searchAgain}</StyledButton>
          <StyledArticle>
          { userError && <h1>{strings.errorFetchingProofile}</h1>}
          </StyledArticle>
        {
          !userError && Object.keys(otherDetails).map(detail => {
           return <StyledArticle key={detail}>
               <StyledH4>{formatKey(detail)}: </StyledH4>
              <StyledP>{otherDetails[detail]}</StyledP>
           </StyledArticle>
          })
        }    
        </StyledDetailsSection>}
    </>
  )
}

User.propTypes = {
  avatar_url: PropTypes.string,
  login: PropTypes.string,
  showDetails: PropTypes.bool,
  userError: PropTypes.bool,
  otherDetails: PropTypes.shape({
    name: PropTypes.string,
    biography: PropTypes.string,
    location: PropTypes.string,
    followers: PropTypes.number,
    following: PropTypes.number,
    public_gists: PropTypes.number,
    public_repos: PropTypes.number,
    created_at: PropTypes.string
  })
}

User.defaultProps = {
  showDetails: true,
  userError: false,
  login: 'NIL',
  avatar_url: 'https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg'
}

export default React.memo(User)
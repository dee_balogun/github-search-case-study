import styled from 'styled-components'
import { Link } from "react-router-dom";
import { Button } from '../SearchForm/styled';
import { breakPoints } from '../../utils';

export const StyledLink = styled(Link)`
  text-decoration: none;
  margin: 15px 0;

  &:focus {
    background-color: red;
  }
`;

export const StyledFigure = styled.figure`
  background-color: rgb(224 226 231);
  border-radius: 20px;
  display: flex;
  flex-flow: column;
  padding: 5px;
  width: 175px;
  margin: auto;

  img {
    border: 0;
  };

  @media screen and ((min-width: ${breakPoints[1]}px) and (max-width: ${breakPoints[2]}px)) {
    width: 165px;
  };


  @media screen and ((min-width: ${breakPoints[0]}px) and (max-width: ${breakPoints[1]}px)) {
    width: 135px;
  };

  @media screen and (max-width: ${breakPoints[0]}px) {
    width: 105px;
  }
`;

export const StyledFigureCaption = styled.figcaption`
    background-color: #6a6ab1;
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    margin: 0;
    text-align: center;
    color: white;
    line-height: 30px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 15px;
`;

export const StyledDetailsSection = styled.section`
  background-color: #f4f4f4;
  border-radius: 20px;
  width: 100%;
  padding: 20px;
  margin-top: 20px;
  display: flex;
  flex-direction: column;
`;

export const StyledArticle = styled.article`
  margin: 10px 0;
  border-bottom: 1px solid black;
  display: flex;
  align-items: center;
`;

export const StyledH4 = styled.h4`
  margin: 0;
  width: 150px;
`

export const StyledP = styled.p`
  width: calc(100% - 100px);
`

export const StyledButton = styled(Button)`
  align-self: flex-end;
`

export const StyledImage = styled.img`
border-radius: 20px;
`


import styled from 'styled-components'
import FormField from "../../components/FormField";
import { breakPoints } from '../../utils';

export const StyledFormField= styled(FormField)`
  height: 30px;
  width: 100%;
  border-radius: 15px;
  padding: 10px;
`;

export const Button = styled.button`
  border:none;
  width: 100px;
  margin-top: 10px;
  height: 50px;
  border-radius: 10px;
  background: #6a6ab1;
  color: white;
  cursor: pointer;

  &:disabled {
    background: #c3bebe;
    color: black;
    cursor: not-allowed;
  }

  @media screen and (max-width: 600px) {
    width: 100%;
  }
`;

export const StyledSection = styled.section`
  margin-top: 30px;
  margin-bottom: 10px;
  display: flex;

  @media screen and (max-width: 600px) {
    flex-direction: column;
    align-items: center;

    label:first-child {
      margin-bottom: 20px;
    }
  }
`

export const StyledForm = styled.form`
  width: 60%;

  @media screen and (max-width: ${breakPoints[2]}px) {
    width: 90%;
  }
`

export const StyledFormFieldWrapper = styled.section`
  display: flex;
`

export const StyledLabel = styled.label`
  font-size: 18px;
`
import React, { useState, useCallback, useEffect } from "react";
import { useParams } from "react-router-dom";
import { searchTypes } from "../../utils";
import FormField from "../../components/FormField";
import strings from "../../locales/strings";
import {
  StyledFormField,
  Button,
  StyledSection,
  StyledForm,
  StyledFormFieldWrapper,
  StyledLabel
} from './styled'

const SearchForm = ({ onSearch }) => {
  const params = useParams()
  const [searchQuery, setSearchQuery] = useState({
      searchText: params.searchText || '',
      searchType: params.searchType || ''
    })

  useEffect(() => {
      const {searchText, searchType } = params ?? {}
      if (searchText && searchType) {
        setSearchQuery({searchText, searchType})
      }
  }, [params, setSearchQuery])

  const handleChange = useCallback((eventData) => {
    const { target: { name, value } } = eventData
    setSearchQuery(prev => ({
      ...prev,
      [name]: value
    }))
  }, [])

  const search = (e) => {
    e.preventDefault()
    onSearch(searchQuery)
  }
  return(
  <StyledForm
    data-testid='form'
    onSubmit={search}>
    <StyledSection>
      <StyledLabel>{strings.selectFieldToSearch}</StyledLabel>
      <StyledFormFieldWrapper>
        <FormField
          data-testid='users-radio-button'
          type='radio'
          name='searchType'
          value={searchTypes.users.id}
          label={searchTypes.users.label}
          checked={searchQuery.searchType === searchTypes.users.id}
          onChange={handleChange}
        />
        <FormField
          data-testid='orgs-radio-button'
          type='radio'
          name='searchType'
          value={searchTypes.orgs.id}
          checked={searchQuery.searchType === searchTypes.orgs.id}
          label={searchTypes.orgs.label}
          onChange={handleChange}
        />
      </StyledFormFieldWrapper>
    </StyledSection>
    <StyledFormField 
      data-testid='search'
      type='text'
      name='searchText'
      value={searchQuery.searchText}
      onChange={handleChange}
      placeholder='Search'
      />
    <Button
      data-testid='search-button'
      aria-label="Search"
      tabIndex='0'
      type="submit"
      name='Search'
      disabled={!(searchQuery.searchText && searchQuery.searchType)}>{strings.search}</Button>
  </StyledForm>
  )
}

export default React.memo(SearchForm)
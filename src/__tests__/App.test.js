import { fireEvent, render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import React from 'react';
import strings from '../locales/strings';
import Home from '../views/Home';

const state = {
  searchParams: {
  searchQuery: {} 
  }
}

jest.mock('react-router-dom', () => (
  { useNavigate: () => jest.fn(),
    useParams: () =>  ({
      searchText: '',
      searchType: ''
    }),
    useSearchParams: () => [{
      get: () => {}
    }, jest.fn],
    Outlet: () => <div></div>
}));

jest.mock('react-redux', () => (
  { useDispatch: () => jest.fn,
    useSelector: () => state
}));

describe('Render Home Component', () => {

  const renderComponent = () =>render(<Home/>)
  
  test('it displays all texts', () => {
    renderComponent();

    expect(screen.getByText(strings.githubSearch)).toBeInTheDocument();
    expect(screen.getByText(strings.appDescription)).toBeInTheDocument();
    expect(screen.getByText(strings.selectFieldToSearch)).toBeInTheDocument();
  })

  test('it renders form elements', async () => {
      
    renderComponent();

      const usersRadioButton = await screen.findByTestId('users-radio-button')
      const orgsRadioButton = await screen.findByTestId('orgs-radio-button')
      const searchInput = await screen.findByTestId('search')

      expect(usersRadioButton).toBeInTheDocument()
      expect(orgsRadioButton).toBeInTheDocument()
      expect(usersRadioButton).not.toHaveAttribute('checked')
      expect(orgsRadioButton).not.toHaveAttribute('checked')
      expect(searchInput).toBeInTheDocument()
      expect(searchInput).toHaveAttribute('placeholder', 'Search')
      expect(searchInput).toHaveAttribute('name', 'searchText')
  })

  test('changes search type and text', async () => {

  const state = {
      searchText: 'text',
      searchType: 'users'
  }
      
    renderComponent();

      const usersRadioButton = await screen.findByTestId('users-radio-button')
      const orgsRadioButton = await screen.findByTestId('orgs-radio-button')
      const searchInput = await screen.findByTestId('search')


      fireEvent.click(usersRadioButton)

      expect(usersRadioButton).toBeChecked()
      expect(usersRadioButton).toHaveAttribute('value', 'users')
      expect(orgsRadioButton).not.toBeChecked()

      fireEvent.click(orgsRadioButton)

      expect(orgsRadioButton).toBeChecked()
      expect(orgsRadioButton).toHaveAttribute('value', 'orgs')
      expect(usersRadioButton).not.toBeChecked()

      fireEvent.change(searchInput, { target: { value: state.searchText } })
      expect(searchInput.value).toBe(state.searchText)
      
  })
});

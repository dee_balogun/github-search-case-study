import React from 'react'
import strings from '../locales/strings'

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      hasError: false
    }
  }

  static getDerivedStateFromError = (error) => {
      return { hasError: true }
  }

  render() {
    const { hasError } = this.state
    return hasError ? <h1>{strings.appError}</h1> : this.props.children
  }
}

export default ErrorBoundary
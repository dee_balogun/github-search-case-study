import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import User from "../../components/User";
import strings from "../../locales/strings";
import { fetchDetails } from "../../store/actions";
import { RestyledBaseSection, StyledSection } from "./styled";

const Profile = () => {
  const {
    loading,
    message,
    userDetails: stateUserDetails,
    currentUsername
  } = useSelector(state => state)
  const params = useParams()
  const dispatch = useDispatch()
  const name = currentUsername || params.username

  useEffect(() => {
    if (!stateUserDetails[name]) {
        dispatch(fetchDetails(name))
    } 
  }, [currentUsername, name, dispatch, stateUserDetails])

  const details = stateUserDetails[name]

  return(
    <RestyledBaseSection>
      {
        loading ? <h2>{strings.fetching(name)}</h2>
        :
          <StyledSection>
            <User
              userError={Boolean(message)}
              avatar_url={details?.avatar_url}
              login={details?.name}
              otherDetails={{
                name: details?.name,
                biography: details?.bio,
                location: details?.location,
                followers: details?.followers,
                following: details?.following,
                public_gists: details?.public_gists,
                public_repos: details?.public_repos,
                created_at: details?.created_at,
              }}
            />
          </StyledSection>
      }
    </RestyledBaseSection>
  )
}

export default Profile;
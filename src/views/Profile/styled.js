import styled from 'styled-components'
import { StyledBaseSection } from '../../components'

export const RestyledBaseSection = styled(StyledBaseSection)`
  display: flex;
  justify-content: center;
  height: 100vh;
`

export const StyledSection = styled.section`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 50%;

  @media screen and (max-width: 600px) {
    width: 100%
  }
`
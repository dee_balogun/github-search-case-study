import styled from 'styled-components'
import { breakPoints } from '../../utils';

export const StyledSection = styled.section`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  width: 80%;
`;

export const StyledOutletWrapper = styled.section`
  display: flex;
  flex-wrap: wrap;
  width: 60%;
  marginTop: 30px;
  justify-content: center;

  @media screen and (max-width: ${breakPoints[2]}px) {
    width: 90%;
    margin-top: 20px;

    article {
      button[name=next] {
        width: 50px;
      }
    }
  }
`
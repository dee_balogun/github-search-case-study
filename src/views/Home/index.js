import React, { useCallback, useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, Outlet, useParams, useSearchParams } from 'react-router-dom'
import { StyledBaseSection } from "../../components";
import SearchForm from "../../components/SearchForm";
import strings from "../../locales/strings";
import { setSearchParams } from "../../store/actions";
import { pageLimit } from "../../utils";
import { StyledOutletWrapper } from "./styled";

const Home = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const params = useParams()
  const [searchParams] = useSearchParams();
  const { 
    searchParams: {
      searchQuery: stateSearchQuery 
    } 
  } = useSelector(state => state)    
    

  const navigateUsingParams = (params) => {
    dispatch(setSearchParams(params))
    const { searchQuery, pageNumber } = params
      navigate({
        pathname: `${searchQuery.searchType}/${searchQuery.searchText}`,
        search: `?pageNumber=${pageNumber}`
        })
  }

  useEffect(() => {
    const {searchType, searchText } = params ?? {}

    if (searchType || searchText) {     
        const pageNumber = parseInt(searchParams.get('pageNumber'), 10)
        navigateUsingParams({
          searchQuery: { searchType, searchText }, 
          pageNumber: pageNumber || pageLimit.pageNumber,
          reset: true,
        })
      } 
}, [])

  const handleSearch = useCallback((searchQuery) => {
    const { searchType, searchText } = stateSearchQuery ?? {}
    const reset = searchType !== searchQuery.searchType || 
    searchText !== searchQuery.searchText 

    if (!reset) return

      const pageNumberToUse = reset ? pageLimit.pageNumber
        : searchQuery.pageNumber
      navigateUsingParams({
        searchQuery, 
        pageNumber: pageNumberToUse,
        reset: true,
        shouldNavigate: true
      })
  }, [stateSearchQuery])

  return(<StyledBaseSection>
          <h1>{strings.githubSearch}</h1>
          <p>{strings.appDescription}</p>
          <SearchForm
            onSearch={handleSearch}
            />
            <StyledOutletWrapper>
              <Outlet/> 
            </StyledOutletWrapper>
        </StyledBaseSection>
  )
}

export default React.memo(Home)
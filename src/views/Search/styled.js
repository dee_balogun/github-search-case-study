import styled from 'styled-components'
import { Link } from "react-router-dom";
import { Button } from '../../components';

export const StyledLink = styled(Link)`
  text-decoration: none;
  margin: 15px 0;
  cursor: pointer;

  &:hover {
    border: 1px solid #6a6ab1;
  }

  &:active {
    border: 1px solid #6a6ab1;
   },
`;

export const StyledButton = styled.button`
    background-color: transparent;
    border: none;
    cursor: pointer;
   
`

export const StyledNextButton = styled(Button)`
  margin-left: 10px;
   
`

export const StyledResultWrapper = styled.article`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  @media screen and (max-width: 600px) {
    justify-content: center;
  }
`

export const StyledArticleWrapper = styled.article`
  display: flex;
  justify-content: flex-end;
  align-items: center;

`
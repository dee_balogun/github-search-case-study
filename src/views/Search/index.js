import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUsers, setSearchParams as setSearchParamsAction, setUsername } from "../../store/actions";
import { useSearchParams } from 'react-router-dom';
import { StyledLink, StyledButton, StyledResultWrapper, StyledArticleWrapper, StyledNextButton } from "./styled";
import { pageLimit } from "../../utils";
import User from "../../components/User";
import strings from "../../locales/strings";

const Search = () => {
  const {
    users = [],
    loading, 
    totalCount,
    loadNew,
    searchParams: stateSearchParams,
    message } = useSelector(state => state)
    const dispatch = useDispatch()
    const [loadingMore, setLoadingMore] = useState(false)
    const [_, setSearchParams] = useSearchParams();
    const {
      searchQuery: { searchText = '', searchType = '' },
      pageNumber,
      reset
    } = stateSearchParams ?? {}
    useEffect(() => {
       
      if (!loadNew) return 
      if (searchText && searchType) {

        dispatch(getUsers({
          searchQuery: { searchType, searchText },
          pageNumber: pageNumber || pageLimit.pageNumber,
          reset,
          loadingMore
        }))
      }
    }, [
      pageNumber,
      searchText, 
      searchType
    ])
    
    const loadMore = () => {
      setLoadingMore(true)
      dispatch(setSearchParamsAction({
        searchQuery: stateSearchParams.searchQuery,
        pageNumber: pageNumber + 1
      }))
      setSearchParams({
        pageNumber: pageNumber + 1 })
  }

  const gotProfile = useCallback((username) => {
      dispatch(setUsername(username))
      localStorage.setItem('searchParams', JSON.stringify(stateSearchParams))
  }, [dispatch, setUsername])
  
  if (!loading && message && !users.length) {
    return <h1>{strings.errorLoadingUsers}</h1>
  }

  if (!loading && !users.length) {
    return <h1>{strings.noResult}</h1>
  }

  if (loading) {
   return <h1>{strings.loadingText}</h1>  
  }

  const disableButton = loading ||
    message || 
    !users.length ||
    users.length === totalCount
  return(
    <section>
    <StyledArticleWrapper>
      <p>{strings.resultCountText(users.length, totalCount)}</p>
      <StyledNextButton
        aria-label="Next"
        tabindex='0'
        name='next'
        disabled={disableButton}
        onClick={loadMore}
      >{strings.next}</StyledNextButton>
    </StyledArticleWrapper>

    <StyledResultWrapper>
      { users.map(user => {
          const { avatar_url, login } = user
          return(
            <StyledLink to={`/${searchType}/${login}`} key={login}>
              <StyledButton 
                 aria-label={login}
                 tabindex='0'
                 name={login}
                  onClick={() => gotProfile(login)}>
                <User
                  key={login}
                  avatar_url={avatar_url}
                  login={login}
                  showDetails={false}
                />
              </StyledButton>
        </StyledLink>
          )
        })
        }
    </StyledResultWrapper>
    </section>
  )
}

export default React.memo(Search)